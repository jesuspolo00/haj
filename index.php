<?php $url_actual = "https://" . $_SERVER["SERVER_NAME"] . '/';?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>HAJ Soft</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?=$url_actual?>assets/img/favicon.ico" rel="icon">
  <link href="<?=$url_actual?>assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?=$url_actual?>assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="<?=$url_actual?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?=$url_actual?>assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">  <link href="<?=$url_actual?>assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?=$url_actual?>assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="<?=$url_actual?>assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="<?=$url_actual?>assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <link href="<?=$url_actual?>assets/css/style.css" rel="stylesheet">
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center">

      <h1 class="logo me-auto"><a href="<?=$url_actual?>">HAJ Soft</a></h1>

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="#hero">Inicio</a></li>
          <li><a class="nav-link scrollto" href="#about">Nosotros</a></li>
          <li><a class="nav-link scrollto" href="#services">Servicios</a></li>
          <li><a class="nav-link   scrollto" href="#portfolio">Portfolio</a></li>
          <li><a class="nav-link scrollto" href="#team">Nuestro Equipo</a></li>

          <li><a class="nav-link scrollto" href="#contact">Contactanos</a></li>
          <li><a class="getstarted scrollto" href="#about">Empezar</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">

    <div class="container">
      <div class="row">
        <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
          <h1>Soluciones a tu medida</h1>
          <h2>Contamos con personal calificado para desarrollar tus ideas.</h2>
          <div class="d-flex justify-content-center justify-content-lg-start">
            <a href="#about" class="btn-get-started scrollto">Conocenos</a>
            <!-- <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="glightbox btn-watch-video"><i class="bi bi-play-circle"></i><span>Mirar Video</span></a> -->
          </div>
        </div>
        <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
          <img src="<?=$url_actual?>assets/img/hasname.png" class="img-fluid animated " alt="">
        </div>
      </div>
    </div>

  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= Cliens Section ======= -->
    <section id="cliens" class="cliens section-bg">
      <div class="container">

        <div class="row" data-aos="zoom-in">

          <div class="col-lg-6 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <a href="https://grace.hajsoft.co" target="_blank" class="text-decoration-none">
              <img src="<?=$url_actual?>assets/img/clients/gracehoriz.png" class="img-fluid" alt="">
            </a>
          </div>

          <div class="col-lg-6 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <a href="https://gestoradministrativo.royalschool.edu.co" target="_blank" class="text-decoration-none">
              <img src="<?=$url_actual?>assets/img/clients/samihoriz.png" class="img-fluid" alt="">
            </a>
          </div>

          <!--<div class="col-lg-4 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="<?=$url_actual?>assets/img/clients/client-3.png" class="img-fluid" alt="">
          </div> -->

        </div>

      </div>
    </section><!-- End Cliens Section -->

    <!-- ======= About Us Section ======= -->
    <section id="about" class="about">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Nosotros</h2>
        </div>

        <div class="row content">
          <div class="col-lg-6">
            <p>
             El aumento de la tecnológica ha obligado a las empresas a digitalizar sus procesos para que sean más ágiles y eficientes. HAJ hardware & Software crea soluciones de software que destacan por su innovación, calidad y confianza, para satisfacer las necesidades actuales de las empresas.
           </p>
           <ul>
            <li><i class="ri-check-double-line"></i> Nuestra experiencia nos permite desarrollar software adaptado para ti, y tus necesidades.</li>
            <li><i class="ri-check-double-line"></i> Descubre el portafolio de soluciones, que tenemos a tu disposición, para aprobecha al maximo tus recursos.</li>
            <li><i class="ri-check-double-line"></i> Te presentamos las soluciones especializadas de HAJ Soft para atender las necesidades puntuales de tu compañía.</li>
          </ul>
        </div>
        <div class="col-lg-6 pt-4 pt-lg-0">
          <p>
            HAJ Hardware & Software somos una empresa dedicada a brindarte soluciones de software confiables, innovadoras y de calidad, que se adaptan a las necesidades de tu empresa con el fin de garantizar eficiencia disponibilidad de información y ahorrar tiempo, para las tomas de decisiones.
          </p>
          <a href="#why-us" class="btn-learn-more">Conocenos</a>
        </div>
      </div>

    </div>
  </section><!-- End About Us Section -->

  <!-- ======= Why Us Section ======= -->
  <section id="why-us" class="why-us section-bg">
    <div class="container-fluid" data-aos="fade-up">

      <div class="row">

        <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">

          <div class="content">
            <h3>Convertimos en realidad <strong>tus ideas</strong></h3>
            <p>
              En un ámbito profesional en donde se tienen cantidad de procesos lo que más se busca es facilitar estos mismos, para ello nuestro equipo de trabajo de una manera lógica y profesional se encarga de convertir las ideas que pueden facilitar tus procesos en herramientas que harán más eficientes los procesos de tu compañía.
            </p>
          </div>

          <div class="accordion-list">
            <ul>
              <li>
                <a data-bs-toggle="collapse" class="collapse" data-bs-target="#accordion-list-1"><span>01</span> Estudiamos tus necesidades <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                <div id="accordion-list-1" class="collapse show" data-bs-parent=".accordion-list">
                  <p>
                    Nos encargamos de estudiar tus procesos y requerimientos para poder brindarte la mejor solucion, productos o equipos según lo requieras.
                  </p>
                </div>
              </li>

              <li>
                <a data-bs-toggle="collapse" data-bs-target="#accordion-list-2" class="collapsed"><span>02</span> Implementando las soluciones <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                <div id="accordion-list-2" class="collapse" data-bs-parent=".accordion-list">
                  <p>
                    En el proceso de creacion de tu software o distribucion de tus equipos, estamos en constante contacto contigo para asegurarnos que el proceso va según lo hemos acordado o si deseas algun cambio en el proceso de desarrollo.
                  </p>
                </div>
              </li>

              <li>
                <a data-bs-toggle="collapse" data-bs-target="#accordion-list-3" class="collapsed"><span>03</span> Soporte Técnico Permanente <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                <div id="accordion-list-3" class="collapse" data-bs-parent=".accordion-list">
                  <p>
                    No solo estaremos contigo en el proceso de Desarrollo, sino que también estaremos brindándote acompañamiento una vez que empieces a implementar las herramientas brindadas para asegurarnos de su perfecto funcionamiento.
                  </p>
                </div>
              </li>

            </ul>
          </div>

        </div>

        <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style='background-image: url("<?=$url_actual?>assets/img/guy.png");' data-aos="zoom-in" data-aos-delay="150">&nbsp;</div>
      </div>

    </div>
  </section><!-- End Why Us Section -->

  <!-- ======= Skills Section ======= -->
  <section id="skills" class="skills">
    <div class="container" data-aos="fade-up">

      <div class="row">
        <div class="col-lg-6 d-flex align-items-center" data-aos="fade-right" data-aos-delay="100">
          <img src="<?=$url_actual?>assets/img/skills.png" class="img-fluid" alt="">
        </div>
        <div class="col-lg-6 pt-4 pt-lg-0 content" data-aos="fade-left" data-aos-delay="100">
          <h3>Nuestro personal está capacitado de manera integral</h3>
          <p class="fst-italic">
            Esto con la intención de que sea más ágil y fácil el proceso de implementación de las herramientas que requieras en tus procesos.
            <br/>
            Por eso, estamos capacitados en las siguientes competencias:
          </p>

          <div class="skills-content">

            <div class="progress">
              <span class="skill">HTML <i class="val">100%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

            <div class="progress">
              <span class="skill">CSS <i class="val">90%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

            <div class="progress">
              <span class="skill">JavaScript <i class="val">75%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

            <div class="progress">
              <span class="skill">PHP <i class="val">90%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

          </div>

        </div>
      </div>

    </div>
  </section><!-- End Skills Section -->

  <!-- ======= Services Section ======= -->
  <section id="services" class="services section-bg">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <h2>Nuestros Servicios</h2>
        <p>Trabajamos con las últimas tecnologías para crear soluciones software innovadoras y a tu medida.</p>
      </div>

      <div class="row">
        <div class="col-xl-3 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
          <div class="icon-box">
            <div class="icon"><img src="<?=$url_actual?>assets/img/services/develop.jpg" class="img-fluid" alt=""></div>
            <h4><a href="">Desarrollo de Software</a></h4>
            <p>Contamos con equipos Full Stack para el desarrollo de front-end, back-end. Ofrecemos una alta flexibilidad y capacidad de adaptación a nuestros software, garantizando que aprovecha al maximo nuestros desarrollos.</p>
          </div>
        </div>

        <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in" data-aos-delay="200">
          <div class="icon-box">
            <div class="icon"><img src="<?=$url_actual?>assets/img/services/database.jpg" class="img-fluid" alt=""></div>
            <h4><a href="">Gestión de los Datos</a></h4>
            <p>Explotamos informacion, convertimos datos complejos en informacion necesaria y clave para potencializar tu negocio.</p>
          </div>
        </div>

        <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-xl-0" data-aos="zoom-in" data-aos-delay="300">
          <div class="icon-box">
            <div class="icon"><img src="<?=$url_actual?>assets/img/services/desing.jpg" class="img-fluid" alt=""></div>
            <h4><a href="">Diseño corporativo</a></h4>
            <p>El diseño corporativo ayuda a las empresas a mostrarse de una manera uniforme y adecuada, generando al mismo tiempo confianza a sus clientes, nosotros te ayudamos.</p>
          </div>
        </div>

        <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-xl-0" data-aos="zoom-in" data-aos-delay="400">
          <div class="icon-box">
            <div class="icon"><img src="<?=$url_actual?>assets/img/services/store.jpg" class="img-fluid" alt=""></div>
            <h4><a href="">Venta de Equipos Tecnológicos</a></h4>
            <p>Tenemos lo que necesitas, computadores, impresoras, suministros y demás.</p>
          </div>
        </div>

      </div>

    </div>
  </section><!-- End Services Section -->

  <!-- ======= Cta Section ======= -->


  <!-- ======= Portfolio Section ======= -->
  <section id="portfolio" class="portfolio">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <h2>Portafolio</h2>
        <p>Nuestros Software y Servicios destacados</p>
      </div>

      <ul id="portfolio-flters" class="d-flex justify-content-center" data-aos="fade-up" data-aos-delay="100">
        <li data-filter=".filter-app" class="filter-active">Software</li>
        <li data-filter=".filter-card">Hardware</li>
      </ul>

      <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">

        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
          <div class="portfolio-img">
            <a href="https://grace.hajsoft.co/" target="_blank" class="text-decoration-none">
            <img src="<?=$url_actual?>assets/img/portfolio/gracevert.png" class="img-fluid" alt="">
            </a>
          </div>
          <div class="portfolio-info">
            <h4>G.R.A.C.E</h4>
            <p>Gestor de Reservas Administrativas Para el Control Empresarial</p>
            <a href="<?=$url_actual?>assets/img/portfolio/gracevert.png" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="App 1"><i class="bx bx-plus"></i></a>
            <a href="#" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
          <div class="portfolio-img">
            <a href="https://gestoradministrativo.royalschool.edu.co/" target="_blank" class="text-decoration-none">
            <img src="<?=$url_actual?>assets/img/portfolio/samivert.png"  class="img-fluid" alt="">
            </a>
          </div>
          <div class="portfolio-info">
            <h4>S.A.M.I.</h4>
            <p>Sistema Administrativo para el manejo Institucional</p>
            <a href="<?=$url_actual?>assets/img/portfolio/samivert.png" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="App 1"><i class="bx bx-plus"></i></a>
            <a href="#" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-card">
          <div class="portfolio-img">
            <img src="<?=$url_actual?>assets/img/portfolio/laptoppng.png" width="100" height="100" class="img-fluid" alt="">
            <img src="<?=$url_actual?>assets/img/portfolio/towerpng.png" width="100" height="100" class="img-fluid" alt="">
            <img src="<?=$url_actual?>assets/img/portfolio/epson.jpg" width="100" height="100" class="img-fluid" alt="">

          </div>
          <div class="portfolio-info">
            <h4>Computadores y Componentes</h4>
            <p>Rgb, de las mejores marcas y Precios</p>
            <a href="<?=$url_actual?>assets/img/portfolio/vostro3405.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
            <a href="#" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-card">
          <div class="portfolio-img">
            <img src="<?=$url_actual?>assets/img/portfolio/headsetpng.png" width="100" height="100" class="img-fluid" alt="">
            <img src="<?=$url_actual?>assets/img/portfolio/mousepng.png" width="100" height="100" class="img-fluid" alt="">
            <img src="<?=$url_actual?>assets/img/portfolio/keypng.png" width="100" height="100" class="img-fluid" alt="">
          </div>
          <div class="portfolio-info">
            <h4>Accesorios y Dispositivos
            </h4>
            <p>Rgb, ergonomicos y de las mejores marcas</p>
            <a href="<?=$url_actual?>assets/img/portfolio/vostro3405.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
            <a href="#" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
          </div>
        </div>







      </div>

    </div>
  </section><!-- End Portfolio Section -->

  <!-- ======= Team Section ======= -->
  <section id="team" class="team section-bg">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <h2>Contamos con un talento comprometido y emprendedor. Promovemos un entorno de trabajo basado en la igualdad de oportunidades, la diversidad y la inclusión.</p>
        </div>

        <div class="row">

          <div class="col-lg-6">
            <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="100">
              <div class="pic"><img src="<?=$url_actual?>assets/img/team/team-1.png" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Angel Vargas Contreras</h4>
                <span>Director Ejecutivo</span>
                <p>Ingeniero de Sistemas</p>
                <div class="social">
                  <a href=""><i class="ri-twitter-fill"></i></a>
                  <a href=""><i class="ri-facebook-fill"></i></a>
                  <a href=""><i class="ri-instagram-fill"></i></a>
                  <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 mt-4 mt-lg-0">
            <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="200">
              <div class="pic"><img src="<?=$url_actual?>assets/img/team/team-3.png" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Jesus Polo</h4>
                <span>Director Ejecutivo</span>
                <p>Tecnologo en Sistemas</p>
                <div class="social">
                  <a href=""><i class="ri-twitter-fill"></i></a>
                  <a href=""><i class="ri-facebook-fill"></i></a>
                  <a href=""><i class="ri-instagram-fill"></i></a>
                  <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 mt-4">
            <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="300">
              <div class="pic"><img src="<?=$url_actual?>assets/img/team/team-2.png" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Heitnner Romero</h4>
                <span>Director Ejecutivo</span>
                <p>Ingeniero de Sistemas</p>
                <div class="social">
                  <a href=""><i class="ri-twitter-fill"></i></a>
                  <a href=""><i class="ri-facebook-fill"></i></a>
                  <a href=""><i class="ri-instagram-fill"></i></a>
                  <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Team Section -->

    <!-- ======= Pricing Section ======= -->




    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Contáctanos</h2>
          <p>Contáctanos para juntos crear y darle solucion a tus necesidades.</p>
        </div>

        <div class="row">

          <div class="col-lg-5 d-flex align-items-stretch">
            <div class="info">

              <div class="email">
                <i class="bi bi-envelope"></i>
                <h4>Email:</h4>
                <p>info.hajsas@gmail.com</p>
              </div>

              <div class="phone">
                <i class="bi bi-phone"></i>
                <h4>Call:</h4>
                <p>+57 300 2268775</p>
              </div>
            </div>

          </div>

          <div class="col-lg-7 mt-5 mt-lg-0 d-flex align-items-stretch">
            <form action="forms/contact.php" method="post" role="form" class="php-email-form">
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="name">Nombre</label>
                  <input type="text" name="name" class="form-control" id="name" required>
                </div>
                <div class="form-group col-md-6">
                  <label for="name">Correo Electronico</label>
                  <input type="email" class="form-control" name="email" id="email" required>
                </div>
              </div>
              <div class="form-group">
                <label for="name">Asunto</label>
                <input type="text" class="form-control" name="subject" id="subject" required>
              </div>
              <div class="form-group">
                <label for="name">Mensaje</label>
                <textarea class="form-control" name="message" rows="10" required></textarea>
              </div>
              <div class="my-3">
                <div class="loading">Cargando</div>
                <div class="error-message"></div>
                <div class="sent-message">Gracias por tu mensaje</div>
              </div>
              <div class="text-center"><button type="submit">Enviar mensaje</button></div>
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-newsletter">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-6">
            <h4>Suscribete para Noticias diarias</h4>
            <p>Mantente al tanto de todos las novedades en los servicios de HAJ Soft</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>HAJ Soft</h3>
            <p>
              Barranquilla, Atlantico<br>
              Colombia <br><br>
              <strong>Telefono</strong>+57 300 2268775<br>
              <strong>Email:</strong> info.hajsas@gmail.com<br>
            </p>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Inicio</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Nosotros</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Servicios</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Terminos de Uso</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Politica de Privacidad</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Nuestros Servicios</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Desarrollo Web</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Diseño Web</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Diseño Grafico</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Nuestras Redes Sociales</h4>
            <p>Mantente en contacto con nosotros</p>
            <div class="social-links mt-3">
              <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
              <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
              <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
              <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
              <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="container footer-bottom clearfix">
      <div class="copyright">
        &copy; Copyright <strong><span>HAJ SOFT</span></strong>. Todos los derechos reservados
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/ -->
        <!-- Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> -->
      </div>
    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?=$url_actual?>assets/vendor/aos/aos.js"></script>
  <script src="<?=$url_actual?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?=$url_actual?>assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="<?=$url_actual?>assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?=$url_actual?>assets/vendor/php-email-form/validate.js"></script>
  <script src="<?=$url_actual?>assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="<?=$url_actual?>assets/vendor/waypoints/noframework.waypoints.js"></script>

  <!-- Template Main JS File -->
  <script src="<?=$url_actual?>assets/js/main.js"></script>

</body>

</html>
